from random import randint

def guessYear():
    return(randint(1924,2004))

def guessMonth():
    return(randint(1,12))

guesses = 1

def get_name():
    print("What is your name?")
    return str(input())

def game(guesses, name, skip, response = None):
    #print("Test: " + str(response))
    if(not skip):
        response = "Guess " + str(guesses) + ": "+ name + " were you born in " + str(guessMonth()) + " / " + str(guessYear())
    print(response)
    answer = input()
    if(guesses < 5):
        if(answer == "yes"):
            print("I did it!")
            return
        elif(answer == "no"):
            guesses += 1
            game(guesses, name, False)
        elif(answer == "exit"):
            print("bye bye " + name)
            return
        else:
            print("I don't understand. Lets try this again")
            game(guesses, name, True, response = response)
    else:
        print("I give up. You win.")
        return

name = get_name()
game(guesses, name, False)
exit()






